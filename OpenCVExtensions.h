//
//  OpenCVExtensions.h
//  OpenCVExtensions
//
//  Created by JiangZhping on 2016/9/29.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//
#ifndef __JZPLIB_ALL__
#define __JZPLIB_ALL__

#include "OpenCVExtensions/AddedOperators.h"
#include "OpenCVExtensions/OpencvMatxExtensions.h"
#include "OpenCVExtensions/MatxGeometry.h"
#include "OpenCVExtensions/CvTypeConversions.h"
#include "OpenCVExtensions/CvComplex.h"
#include "OpenCVExtensions/jzplib_core.h"
#include "OpenCVExtensions/jzplib_camera.h"
#include "OpenCVExtensions/jzplib_draw.h"
#include "OpenCVExtensions/jzplib_geom.h"
#include "OpenCVExtensions/jzplib_image.h"
#include "OpenCVExtensions/jzplib_utilities.h"
#include "OpenCVExtensions/jzplib_analysis.h"

// PoseSolverWrapper 3
#include "OpenCVExtensions/PoseSolverWrapper.h"
#include "OpenCVExtensions/RPP.h"
#include "OpenCVExtensions/Rpoly.h"

// general utilities
#include "OpenCVExtensions/PreferenceManager.h"
#include "OpenCVExtensions/TickTockTimer.h"
#include "OpenCVExtensions/ScreenCameraInfo.h"

#include "OpenCVExtensions/KalmanFilter.h"
#include "OpenCVExtensions/KalmanDataTracker.h"

#include "OpenCVExtensions/PlotManager.h"

#define debugcout(x) std::cout<<#x<<x<<std::endl

#endif
