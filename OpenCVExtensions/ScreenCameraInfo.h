//
//  ScreenCameraInfo.hpp
//  OpenCVExtensions
//
//  Created by JiangZhping on 2017/2/2.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#ifndef ScreenCameraInfo_hpp
#define ScreenCameraInfo_hpp

#include <stdio.h>
#include <opencv2/opencv.hpp>


#ifndef __COCOA_XTENSIONS_HEADERBASE__
#ifndef INCH_TO_MM
#define INCH_TO_MM  25.4
#endif
#ifndef MM_TO_INCH
#define MM_TO_INCH  1.0/25.4
#endif

typedef enum {
	UIDeviceOrientationUnknown,
	UIDeviceOrientationPortrait,            // Device oriented vertically, home button on the bottom
	UIDeviceOrientationPortraitUpsideDown,  // Device oriented vertically, home button on the top
	UIDeviceOrientationLandscapeLeft,       // Device oriented horizontally, home button on the right
	UIDeviceOrientationLandscapeRight,      // Device oriented horizontally, home button on the left
	UIDeviceOrientationFaceUp,              // Device oriented flat, face up
	UIDeviceOrientationFaceDown             // Device oriented flat, face down
} UIDeviceOrientation;
#endif

std::string crossPlatform_exec(const char* cmd);

class ScreenCameraInfo {
public:
	std::string deviceCode;
	std::string productName;
	cv::Size2i screenPhysicalResolution;
	double screenPhysicalPPI;
	cv::Point2d equavalentPixelPointForFrontCamera;
	int frontCameraDeviceId;
	cv::Size2i frontCameraResolution;
	cv::Point2i principalPoint;
	double focalLength;
	cv::Point2d focalCenterDeviation;
	cv::Rect croppingExtentInSymmetricFrame;
	UIDeviceOrientation deviceOrientation;
	// camera related code, mainly by Quanqing
	cv::Matx33d cameraMatrix();
	cv::Matx33d cameraMatrixForFrameSize(cv::Size2i frameSize);
	double cameraXFov();
	double cameraYFov();
	cv::Size2i cameraSymmetricFrameSize();
	double cameraSymmetricXFov();
	double cameraSymmetricYFov();

	static std::string currentDeviceCode();
	static ScreenCameraInfo & getCurrentDiviceInfo();
	
	cv::Size currentDisplayResolution(); 
	cv::Matx33d transformationFromScreenToCamera();
	double currentDisplayRatio();
	double currentDisplayPPI();
	cv::Point2d frontCameraPixelInCurrentResolution();
	cv::Matx44d transformFromFrontCameraFrameToSceneModelCenteredOnScreenCenter();

	double screenRotationForOrientation(UIDeviceOrientation orientation);
	cv::Vec2d screenCoordinateCenterInUIDeviceOrientation(UIDeviceOrientation orientation);
	cv::Vec2d transformPixel(cv::Vec2d pointA, UIDeviceOrientation deviceOrientationA, UIDeviceOrientation deviceOrientationB);
	cv::Vec3d frontCameraPointFromPixel(cv::Vec2d pixel, UIDeviceOrientation deviceOrientation);
	cv::Vec3d frontCameraPointFromPixel(cv::Vec2d pixel);
	cv::Vec2d pixelFromFrontCameraPoint(cv::Vec3d point, UIDeviceOrientation deviceOrientation);
	cv::Vec2d pixelFromFrontCameraPoint(cv::Vec3d point);
	cv::Vec3d sceneModelScreenCenterFramePointFromFrontCamearPoint(cv::Vec3d point);

private:
	ScreenCameraInfo();
	ScreenCameraInfo(const std::string & deviceCode, const std::string & productName, const cv::Size2i & screenPhysicalResolution, double screenPhysicalPPI, const cv::Point2d & equavalentPixelPointForFrontCamera, int frontCameraDeviceId, const cv::Size2i & frontCameraResolution, const cv::Point2i & principalPoint, double focalLength, const cv::Point2d & focalCenterDeviation, UIDeviceOrientation deviceOrientation) : deviceCode{ deviceCode }, productName{ productName }, screenPhysicalResolution{ screenPhysicalResolution }, screenPhysicalPPI{ screenPhysicalPPI }, equavalentPixelPointForFrontCamera{ equavalentPixelPointForFrontCamera }, frontCameraDeviceId{ frontCameraDeviceId }, frontCameraResolution{ frontCameraResolution }, principalPoint{ principalPoint }, focalLength{ focalLength }, focalCenterDeviation{ focalCenterDeviation }, deviceOrientation{ deviceOrientation }{}

};

#endif /* ScreenCameraInfo_hpp */
