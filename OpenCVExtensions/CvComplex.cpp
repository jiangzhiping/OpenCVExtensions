//
//  CvComplex.cpp
//  OpenCVExtensions
//
//  Created by 蒋志平 on 2017/12/6.
//

#include "CvComplex.h"

namespace  cve {
    namespace complex {
        
        cv::Mat1d magnitude(const cv::Mat2d & csi) {
            cv::Mat1d magnitude = cv::Mat1d::zeros(csi.size());
            for(auto i = 0 ; i < csi.total(); i++) {
                magnitude(i) = cv::norm(csi(i));
            }
            
            return magnitude;
        }
        
        cv::Mat1d phase(const cv::Mat2d & csi) {
            cv::Mat1d phase = cv::Mat1d::zeros(csi.size());
            for(auto i = 0 ; i < csi.total(); i++) {
                auto complexNumber = csi(i);
                phase(i) = atan2(complexNumber(1), complexNumber(0));
            }

            return phase;
        }

        cv::Mat1d unwrap(const cv::Mat1d & phase) {
            auto phase_unwrap = [](double prev, double now) -> double
            {
                constexpr double M_2PI = M_PI * 2;
                auto diff = fmod(now - prev, M_PI * 2);
                auto diff_abs = fabs(diff);
                if (M_PI >= diff_abs)
                    return prev + diff;
                else {
                    diff = diff > 0 ? -(M_2PI - diff_abs) : (M_2PI - diff_abs);
                    return prev + diff;
                }
            };


            cv::Mat1d unwrapped_phase = phase.clone();
            for(auto i = 1 ; i < unwrapped_phase.total(); i ++) {
                unwrapped_phase(i) = phase_unwrap(unwrapped_phase(i-1), unwrapped_phase(i));
            }
            return unwrapped_phase;
        }

        cv::Mat1d linSpace(double start, double end, double interval) {
            std::vector<double> result;
            auto count = 0;
            for (double s= start; count < (end - start)/interval; s+=interval, count++) {
                result.push_back(s);
            }
            result.push_back(end);
            return cv::Mat1d(result, true); // <-- must be true for copying data.
        }
    }
}
