//
//  jzplib_utilities.hpp
//  OpenCVExtensions
//
//  Created by JiangZhping on 2017/1/30.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#ifndef jzplib_utilities_hpp
#define jzplib_utilities_hpp

#include <iostream>
#include <chrono>
#include <thread>
#include <numeric>
#include <deque>
#include <map>
#include <string>

using namespace std::chrono;

namespace cve {
    class TickTockTimer {
    private:
        static std::map<std::string, TickTockTimer *> instanceMap;
        std::string timerName;
        std::deque<double> durations;
        int durationLength = 50;
        time_point<steady_clock> startTime;
    public:
        TickTockTimer(const std::string & timerName, int timerLength = 50): timerName{timerName}, durationLength{timerLength} {}
        static TickTockTimer & getInstance(const std::string & timerName, int timerLength = 50);
        TickTockTimer & tick();
        TickTockTimer & tock();
        TickTockTimer & tock_instant();
//        TickTockTimer & tickTockLambda(std::function<void(void)> codeBlock);
    };
}


#endif /* jzplib_utilities_hpp */
