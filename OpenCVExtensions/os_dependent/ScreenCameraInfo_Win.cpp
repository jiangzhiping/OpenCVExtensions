//
//  ScreenCameraInfo_Win.cpp
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2017/2/2.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#include "../ScreenCameraInfo.h"
#include <regex>

std::string ScreenCameraInfo::currentDeviceCode(){
    std::string execResult = crossPlatform_exec("WMIC computersystem get Manufacturer, Model");
    std::regex r1("[\\s]*Manufacturer[\\s]+Model[\\s]+");
    std::regex r2("[\\s]+");
    return std::regex_replace(std::regex_replace(execResult, r1, ""), r2, "");
}
