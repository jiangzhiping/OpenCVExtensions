//
//  PreferenceManager.hpp
//  OpenCVExtensions
//
//  Created by JiangZhping on 2017/1/30.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#ifndef PreferenceManager_hpp
#define PreferenceManager_hpp

#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include "Singleton.h"


class PreferenceManager {
    friend Singleton<PreferenceManager>;
public:
    
    static std::string getInternalDataString() {
        return internalDataString;
    }
    
    static std::string getStorageFilePath() {
        return storageFilePath;
    }
    
    static void setStorageFilePath(const std::string & newPath) {
        storageFilePath = newPath;
    }
    
    static bool isInstantFlush() {
        return instantFlush;
    }
    
    static void setInstantFlush(bool aSwitch) {
        instantFlush = aSwitch;
    }
    
    
    /**
     Check is the key exists in the current preference mananger

     @param stringId the key to look-up
     @return the result bool
     */
    bool keyExists(const std::string & stringId);
    
    
    /**
     Save a key-value pair into the preference manager.
     Since preference manager is based on cv::FileStorage, therefore, the supported value type is identical to FileStorage.

     @param stringKey the key string
     @param value the value to be saved.
     */
    template<typename _Tp> void persist(const std::string & stringKey, const _Tp & value);
    
    
    /**
     Look-up and return the key-ed value saved in the preference mananger. 
     If the key doesn't exist, persist the defaultValue and return the defaultValue

     @param stringKey the key string to look-up
     @param defaultValue the defaultValue in case of non-existent key.
     @return the key-ed value.
     */
    template<typename _Tp> _Tp queueFor(const std::string & stringKey, const _Tp & defaultValue = *(_Tp *)nullptr);
    
    
    /**
     Flush the in-memory preference into disk. The file path is storageFilePath.
     */
    void flushToDisk();
    
private:
    static std::string internalDataString;
    static std::string storageFilePath;
    static bool instantFlush;
    
    PreferenceManager();
    ~PreferenceManager();    
};

typedef Singleton<PreferenceManager> PreferenceManagerSingleton;

template<typename _Tp> void PreferenceManager::persist(const std::string & stringKey, const _Tp & value) {
    
    cv::FileStorage storageReader(internalDataString, cv::FileStorage::READ + cv::FileStorage::MEMORY);
    cv::FileStorage storageWriter(storageFilePath, cv::FileStorage::WRITE + cv::FileStorage::MEMORY);
    for (auto it = storageReader.root().begin(); it != storageReader.root().end(); it++) {
        auto fn = (*it);
        if (fn.name() != stringKey) {
            switch(fn.type()) {
                case cv::FileNode::INT:
                    storageWriter << fn.name() << (int)fn;
                    break;
                case cv::FileNode::FLOAT:
                    storageWriter << fn.name() << (float)fn;
                    break;
                case cv::FileNode::STRING:
                    storageWriter << fn.name() << (std::string)fn;
                    break;
                case cv::FileNode::SEQ: {
                    CvSeq * seq = fn.node->data.seq;
                    CvSeqReader reader;
                    cvStartReadSeq(seq, &reader);
                    std::vector<double> valueCache;
                    for (int i = 0 ; i < seq->total; i ++) {
                        auto x = cvReadReal((CvFileNode *)reader.ptr);
                        valueCache.push_back(x);
                        CV_NEXT_SEQ_ELEM(seq->elem_size, reader);
                    }
                    storageWriter << fn.name() <<"[:";
                    for(auto value: valueCache) {
                        storageWriter << value;
                    }
                    storageWriter << "]";
                }
                    break;
                case cv::FileNode::MAP:
                    if (std::string(fn.node->info->type_name) == std::string("opencv-matrix")) {
                        cv::Mat bMat;
                        storageReader[ fn.name() ] >> bMat;
                        storageWriter << fn.name() << bMat;
                        break;
                    }
            }
        } else {
            storageWriter << stringKey << value;
        }
    }
    if (storageReader[stringKey].isNone()) {
        storageWriter << stringKey << value;
    }
    internalDataString = storageWriter.releaseAndGetString();
    if (instantFlush) {
        this->flushToDisk();
    }
}

template<typename _Tp> _Tp PreferenceManager::queueFor(const std::string & stringKey, const _Tp & defaultValue) {
    cv::FileStorage storageReader(PreferenceManager::internalDataString, cv::FileStorage::READ + cv::FileStorage::MEMORY);
    if (!storageReader[stringKey].isNone()) {
        _Tp returnValue; storageReader[stringKey] >> returnValue;
        storageReader.release();
        return returnValue;
    } else {
        persist(stringKey, defaultValue);
        return queueFor(stringKey, defaultValue);
    }
}

#endif /* PreferenceManager_hpp */
