//
//  ScreenCameraInfo.cpp
//  OpenCVExtensions
//
//  Created by JiangZhping on 2017/2/2.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#include "ScreenCameraInfo.h"
#include <stdio.h>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include "jzplib_geom.h"
std::string crossPlatform_exec(const char* cmd) {
	std::array<char, 128> buffer;
	std::string result;
#ifdef _WIN32
	std::shared_ptr<FILE> pipe(_popen(cmd, "r"), _pclose);
#else
	std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
#endif
	if (!pipe) throw std::runtime_error("popen() failed!");
	while (!feof(pipe.get())) {
		if (fgets(buffer.data(), 128, pipe.get()) != NULL)
			result += buffer.data();
	}
	return result;
}

ScreenCameraInfo & ScreenCameraInfo::getCurrentDiviceInfo() {
	
    static bool initialized = false;
    static ScreenCameraInfo * instance;
    if (initialized) {
        return *instance;
    }
    
	std::string currentDeviceCode = ScreenCameraInfo::currentDeviceCode();

	std::string deviceCode;
	std::string productName;
	cv::Size2i screenPhysicalResolution;
	double screenPhysicalPPI = 0.0;
	int frontCameraDeviceId;
	cv::Point2d equavalentPixelPointForFrontCamera;
	cv::Size2i frontCameraResolution;
	cv::Point2i principalPoint;
	double focalLength = 0.0;
	cv::Point2d focalCenterDeviation;
	UIDeviceOrientation deviceOrientation = UIDeviceOrientationPortrait;
	cv::FileStorage fs("deviceInfo.yml", cv::FileStorage::READ);
	cv::FileNode deviceNode = fs[currentDeviceCode];
	deviceCode = currentDeviceCode;
	deviceNode["productName"] >> productName;
	deviceNode["screenPhysicalResolution"] >> screenPhysicalResolution;
	deviceNode["screenPhysicalPPI"] >> screenPhysicalPPI;
	deviceNode["frontCameraDeviceId"] >> frontCameraDeviceId;
	deviceNode["equavalentPixelPointForFrontCamera"] >> equavalentPixelPointForFrontCamera;
	deviceNode["frontCameraResolution"] >> frontCameraResolution;
	deviceNode["principalPoint"] >> principalPoint;
	deviceNode["focalLength"] >> focalLength;
	deviceNode["focalCenterDeviation"] >> focalCenterDeviation;
	std::string deviceOrientationString;
	deviceNode["deviceOrientation"] >> deviceOrientationString;
	if (deviceOrientationString.compare("UIDeviceOrientationUnknown") == 0){
		deviceOrientation = UIDeviceOrientationUnknown;
		std::cout << "dasd" << deviceOrientationString;
	}
	else if (deviceOrientationString.compare("UIDeviceOrientationPortrait") == 0){
		deviceOrientation = UIDeviceOrientationPortrait;
	}
	else if (deviceOrientationString.compare("UIDeviceOrientationPortraitUpsideDown") == 0){
		deviceOrientation = UIDeviceOrientationPortraitUpsideDown;
	}
	else if (deviceOrientationString.compare("UIDeviceOrientationLandscapeLeft") == 0){
		deviceOrientation = UIDeviceOrientationLandscapeLeft;
	}
	else if (deviceOrientationString.compare("UIDeviceOrientationLandscapeRight") == 0){
		deviceOrientation = UIDeviceOrientationLandscapeRight;
	}
	else if (deviceOrientationString.compare("UIDeviceOrientationFaceUp") == 0){
		deviceOrientation = UIDeviceOrientationFaceUp;
	}
	else if (deviceOrientationString.compare("UIDeviceOrientationFaceDown") == 0){
		deviceOrientation = UIDeviceOrientationFaceDown;
	}

	// Quanqing, these lines are just temporary.
	/*if (currentDeviceCode.find("Surface") != std::string::npos) {
	frontCameraDeviceId = 1;
	frontCameraResolution = cv::Size2i(1920,1440);
	focalLength = 1500;
	} else if (currentDeviceCode.find("MacBook")  != std::string::npos) {
	frontCameraDeviceId = 0;
	frontCameraResolution = cv::Size2i(1280,720);
	focalLength = 1000;
	}*/

	instance = new ScreenCameraInfo(deviceCode, productName, screenPhysicalResolution, screenPhysicalPPI, equavalentPixelPointForFrontCamera, frontCameraDeviceId, frontCameraResolution, principalPoint, focalLength, focalCenterDeviation, deviceOrientation);
    initialized = true;
    
    return *instance;
}

cv::Matx33d ScreenCameraInfo::cameraMatrix() {
	return cameraMatrixForFrameSize(frontCameraResolution);
}

cv::Matx33d ScreenCameraInfo::cameraMatrixForFrameSize(cv::Size2i frameSize) {
	cv::Point2d frameCenter = cv::Point2d(frameSize.width / 2.0, frameSize.height / 2.0);
	cv::Point2d deviationInCurrentSize = cv::Point2d(frameSize.width > frameSize.height ? focalCenterDeviation : cv::Point2d(focalCenterDeviation.y, focalCenterDeviation.x))*(MAX(frameSize.width, frameSize.height) / MAX(frontCameraResolution.width, frontCameraResolution.height));
	cv::Point2d focalPoint = frameCenter;
	switch (deviceOrientation) {
	case UIDeviceOrientationPortrait:
	case UIDeviceOrientationLandscapeLeft:
		focalPoint = frameCenter + deviationInCurrentSize;
		croppingExtentInSymmetricFrame = cv::Rect(cv::Point(0, 0), frameSize);
		break;
	case UIDeviceOrientationLandscapeRight:
	case UIDeviceOrientationPortraitUpsideDown:
		focalPoint = frameCenter + cv::Point2d(-deviationInCurrentSize.x, -deviationInCurrentSize.y);
		croppingExtentInSymmetricFrame = cv::Rect(deviationInCurrentSize*2.0, frameSize);
		break;
	default:
		focalPoint = frameCenter;
		croppingExtentInSymmetricFrame = cv::Rect(cv::Point(0, 0), frameSize);
	}

	if (deviceOrientation == UIDeviceOrientationPortrait || deviceOrientation == UIDeviceOrientationPortraitUpsideDown) {
		focalPoint.y = frameSize.height - focalPoint.y;
	}

	double newFocalLength = (frameSize.width > frameSize.height ? frameSize.width : frameSize.height) * focalLength/ (frontCameraResolution.width > frontCameraResolution.height ? frontCameraResolution.width : frontCameraResolution.height);
	cv::Matx33d cameraMatrix = cv::Matx33d(newFocalLength, 0, focalPoint.x, 0, newFocalLength, focalPoint.y, 0, 0, 1);

	return cameraMatrix;
}

double ScreenCameraInfo::cameraXFov() {
	cv::Matx33d cameraMatrix = ScreenCameraInfo::cameraMatrix();
	return cve::rad2deg(atan2(cameraMatrix(2, 0), cameraMatrix(0, 0)) + atan2(frontCameraResolution.width - cameraMatrix(2, 0), cameraMatrix(0, 0)));
}

double ScreenCameraInfo::cameraYFov() {
	cv::Matx33d cameraMatrix = ScreenCameraInfo::cameraMatrix();
	return cve::rad2deg(atan2(cameraMatrix(2, 1), cameraMatrix(1, 1)) + atan2(frontCameraResolution.height - cameraMatrix(2, 1), cameraMatrix(1, 1)));
}

cv::Size2i ScreenCameraInfo::cameraSymmetricFrameSize() {
	cv::Matx33d cameraMatrix = ScreenCameraInfo::cameraMatrix();
	double maxWidth = 2.0 * MAX(cameraMatrix(2, 0), frontCameraResolution.width - cameraMatrix(2, 0));
	double maxHeight = 2.0 * MAX(cameraMatrix(2, 1), frontCameraResolution.height - cameraMatrix(2, 1));
	return cv::Size2i(maxWidth, maxHeight);
}

double ScreenCameraInfo::cameraSymmetricXFov() {
	cv::Matx33d cameraMatrix = ScreenCameraInfo::cameraMatrix();
	cv::Size2i symmetricFrameSize = cameraSymmetricFrameSize();
	return cve::rad2deg(2.0 * atan2(symmetricFrameSize.width / 2.0, cameraMatrix(0, 0)));
}

double ScreenCameraInfo::cameraSymmetricYFov() {
	cv::Matx33d cameraMatrix = ScreenCameraInfo::cameraMatrix();
	cv::Size2i symmetricFrameSize = cameraSymmetricFrameSize();
	return cve::rad2deg(2.0 * atan2(symmetricFrameSize.height / 2.0, cameraMatrix(1, 1)));
}
cv::Size ScreenCameraInfo::currentDisplayResolution() {
	return screenPhysicalResolution;
}
cv::Matx33d ScreenCameraInfo::transformationFromScreenToCamera() {
	return cv::Matx33d(-1, 0, 0, 0, 1, 0, 0, 0, -1);
}
double ScreenCameraInfo::currentDisplayRatio() {
	float currentDisplayMaxLength = currentDisplayResolution().width > currentDisplayResolution().height ? currentDisplayResolution().width : currentDisplayResolution().height;
	float physicalDisplayMaxLength = screenPhysicalResolution.width > screenPhysicalResolution.height ? screenPhysicalResolution.width : screenPhysicalResolution.height;
	return 1.f * currentDisplayMaxLength / physicalDisplayMaxLength;
}
double ScreenCameraInfo::currentDisplayPPI() {
	double currentDisplayRatio = ScreenCameraInfo::currentDisplayRatio();
	return screenPhysicalPPI * currentDisplayRatio;
}
cv::Point2d ScreenCameraInfo::frontCameraPixelInCurrentResolution() {
	double currentDisplayRatio = ScreenCameraInfo::currentDisplayRatio();
	return equavalentPixelPointForFrontCamera*currentDisplayRatio;
}
cv::Matx44d ScreenCameraInfo::transformFromFrontCameraFrameToSceneModelCenteredOnScreenCenter() {
	cv::Vec2d screenCenterPixel = cv::Vec2d(currentDisplayResolution().width, currentDisplayResolution().height)*0.5;
	cv::Vec3d screenCenterInCameraFrame = frontCameraPointFromPixel(screenCenterPixel);
	return cve::matx::joinRotationMatAndTranslationVec(cv::Matx33d::eye(), screenCenterInCameraFrame)*cve::matx::joinRotationMatAndTranslationVec(cve::matx::rotationMatrixFromEulerAngles(0.0, 0.0, -CV_PI), cv::Vec3d(0, 0, 0));	
}	

double ScreenCameraInfo::screenRotationForOrientation(UIDeviceOrientation orientation) {
	double radianValue = 0;
	switch (orientation) {
	case UIDeviceOrientationPortrait:
		radianValue = 0;
		break;
	case UIDeviceOrientationLandscapeLeft:
		radianValue = CV_PI / 2.0;
		break;
	case UIDeviceOrientationLandscapeRight:
		radianValue = -CV_PI / 2.0;
		break;
	case UIDeviceOrientationPortraitUpsideDown:
		radianValue = CV_PI;
		break;
	default:
		radianValue = 0;
		break;
	}
	return radianValue;
}
cv::Vec2d ScreenCameraInfo::screenCoordinateCenterInUIDeviceOrientation(UIDeviceOrientation orientation) {
	cv::Vec2d  resultVector;
	switch (orientation) {
	case UIDeviceOrientationPortrait:
		resultVector = cv::Vec2d(0, 0);
		break;
	case UIDeviceOrientationLandscapeLeft:
		resultVector = cv::Vec2d(screenPhysicalResolution.width, 0);
		break;
	case UIDeviceOrientationLandscapeRight:
		resultVector = cv::Vec2d(screenPhysicalResolution.width, 0);
		break;
	case UIDeviceOrientationPortraitUpsideDown:
		resultVector = cv::Vec2d(screenPhysicalResolution.width, screenPhysicalResolution.height);
		break;
	default:
		resultVector = cv::Vec2d(0, 0);
	}
	return resultVector*currentDisplayRatio();
}
cv::Vec2d ScreenCameraInfo::transformPixel(cv::Vec2d pointA, UIDeviceOrientation deviceOrientationA, UIDeviceOrientation deviceOrientationB) {
	cv::Vec2d zeroPointForA = screenCoordinateCenterInUIDeviceOrientation(deviceOrientationA);
	cv::Vec2d zerosPointForB = screenCoordinateCenterInUIDeviceOrientation(deviceOrientationB);

	double screenAngleForA = screenRotationForOrientation(deviceOrientationA);
	double screenAngleForB = screenRotationForOrientation(deviceOrientationB);
	// first transform to standard portaint.
	cv::Vec2d pointInPortaint = cve::matx::transformVectorWithTransformMat(cve::matx::rotationMatrixFromEulerAngles(0.0,0.0,-screenAngleForA), pointA)+zeroPointForA;
	pointInPortaint = pointInPortaint- zerosPointForB;	
	cv::Vec2d pointInB = cve::matx::transformVectorWithTransformMat(cve::matx::rotationMatrixFromEulerAngles(0.0, 0.0, screenAngleForB), pointInPortaint);
	return pointInB;
}
cv::Vec3d ScreenCameraInfo::frontCameraPointFromPixel(cv::Vec2d pixel, UIDeviceOrientation deviceOrientation) {
	// first obtain the inver rotation angle.
	double inverseRotationRadian = screenRotationForOrientation(deviceOrientation);
	// obtain the Zero (in the given device orientation)'s position in standard portait coordinate system.
	cv::Vec2d coordinateZeroInPortraitScreen = screenCoordinateCenterInUIDeviceOrientation(deviceOrientation);
	// then obtain the given pixel's cooridnate in portait view.
	cv::Vec2d pixelCoordinateInPortraitScreen = cve::matx::transformVectorWithTransformMat(cve::matx::rotationMatrixFromEulerAngles(0.0, 0.0, inverseRotationRadian), pixel)+ coordinateZeroInPortraitScreen;
	// the pixel distance to the camera
	pixelCoordinateInPortraitScreen = pixelCoordinateInPortraitScreen- cv::Vec2d(frontCameraPixelInCurrentResolution());
	// rotate the pixel_to_camera into portait view
	cv::Vec2d pixelInPortraitCameraFrame = cve::matx::transformVectorWithTransformMat(transformationFromScreenToCamera(), pixelCoordinateInPortraitScreen);
	// then rotate to the current camera orientation,
	// note that the rotation angle is inversed.
	cv::Vec2d pixelInCameraFrame = cve::matx::transformVectorWithTransformMat(cve::matx::rotationMatrixFromEulerAngles(0.0, 0.0, inverseRotationRadian), pixelInPortraitCameraFrame);
	// transformed into milimeter.
	pixelInCameraFrame = pixelInCameraFrame/currentDisplayPPI();
	pixelInCameraFrame =pixelInCameraFrame*INCH_TO_MM;
	return cve::vec::changeDimension<3>(pixelInCameraFrame,0.0);
}
cv::Vec3d ScreenCameraInfo::frontCameraPointFromPixel(cv::Vec2d pixel) {
		UIDeviceOrientation currentOrientation = UIDeviceOrientationPortrait;
		return frontCameraPointFromPixel(pixel,currentOrientation);
}
cv::Vec2d ScreenCameraInfo::pixelFromFrontCameraPoint(cv::Vec3d point, UIDeviceOrientation deviceOrientation) {
	cv::Vec2d frontCameraPixel2D = cve::vec::changeDimension<2>(point);
	frontCameraPixel2D = frontCameraPixel2D*(MM_TO_INCH * currentDisplayPPI());
	frontCameraPixel2D = cve::matx::transformVectorWithTransformMat(transformationFromScreenToCamera().t(), frontCameraPixel2D);
	double inverseRotationRadian = -screenRotationForOrientation(deviceOrientation);
	// convert to portrait camera view
	cv::Vec2d pixelInPortraitFrontCamera = cve::matx::transformVectorWithTransformMat(cve::matx::rotationMatrixFromEulerAngles(0.0, 0.0, inverseRotationRadian), frontCameraPixel2D);
	cv::Vec2d pointInPortraitScreen = cv::Vec2d(frontCameraPixelInCurrentResolution())+ pixelInPortraitFrontCamera;
	// get screen pixel in portrait view
	cv::Vec2d pointInTargetOrientation = transformPixel(pointInPortraitScreen, UIDeviceOrientationPortrait, deviceOrientation);
	return pointInTargetOrientation;
}
cv::Vec2d ScreenCameraInfo::pixelFromFrontCameraPoint(cv::Vec3d point) {
	UIDeviceOrientation currentOrientation = UIDeviceOrientationPortrait;
	return pixelFromFrontCameraPoint(point,currentOrientation);
}
cv::Vec3d ScreenCameraInfo::sceneModelScreenCenterFramePointFromFrontCamearPoint(cv::Vec3d point) {
	return cve::matx::transformVectorWithTransformMat(transformFromFrontCameraFrameToSceneModelCenteredOnScreenCenter(), point);
}
