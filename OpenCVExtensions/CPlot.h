    //
    //  CPlot.hpp
    //  dlibxx
    //
    //  Created by 蒋志平 on 2017/12/6.
    //

#ifndef CPlot_h
#define CPlot_h

#include <opencv2/opencv.hpp>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <math.h>
#define WINDOW_WIDTH 600
#define WINDOW_HEIGHT 600
using namespace cv;
using namespace std;

struct LineType
{
    char type;
    bool is_need_lined;
    Scalar color;
};

class CPlot
{
public:
    void DrawAxis(IplImage *image);
    void DrawData(IplImage *image);
    int window_height;
    int window_width;
    
    
    vector< vector<CvPoint2D64f> >dataset;
    vector<LineType> lineTypeSet;
    CvScalar backgroud_color;
    CvScalar axis_color;
    CvScalar text_color;
    
    IplImage* Figure;
    
        // manual or automatic range
    bool custom_range_y;
    double y_max;
    double y_min;
    double y_scale;
    
    bool custom_range_x;
    double x_max;
    double x_min;
    double x_scale;
    
        //±ﬂΩÁ¥Û–°
    int border_size;
    
    template<class T> void plot(T *y, size_t Cnt, CvScalar color, char type = '*', bool is_need_lined = true);
    template<class T> void plot(T *x, T *y, size_t Cnt, CvScalar color, char type = '*', bool is_need_lined = true);
    
    void xlabel(string xlabel_name, CvScalar label_color);
    void ylabel(string ylabel_name, CvScalar label_color);
        //«Âø’Õº∆¨…œµƒ ˝æ›
    void clear();
    void title(string title_name, CvScalar title_color);
    
    CPlot();
    ~CPlot();
    
};

    //∑∂–Õ…Ëº∆
template<class T>
void CPlot::plot(T *X, T *Y, size_t Cnt, CvScalar color, char type, bool is_need_lined)
{
        //∂‘ ˝æ›Ω¯––¥Ê¥¢
    T tempX, tempY;
    vector<CvPoint2D64f>data;
    for (int i = 0; i < Cnt; i++)
    {
        tempX = X[i];
        tempY = Y[i];
        data.push_back(cvPoint2D64f((double)tempX, (double)tempY));
    }
    this->dataset.push_back(data);
    LineType LT;
    LT.type = type;
    LT.color = color;
    LT.is_need_lined = is_need_lined;
    this->lineTypeSet.push_back(LT);
    
        //printf("data count:%d\n", this->dataset.size());
    
    this->DrawData(this->Figure); //√ø¥Œ∂º «÷ÿ–¬ªÊ÷∆
}

template<class T>
void CPlot::plot(T *Y, size_t Cnt, CvScalar color, char type, bool is_need_lined)
{
        //∂‘ ˝æ›Ω¯––¥Ê¥¢
    T tempX, tempY;
    vector<CvPoint2D64f>data;
    for (int i = 0; i < Cnt; i++)
    {
        tempX = i;
        tempY = Y[i];
        data.push_back(cvPoint2D64f((double)tempX, (double)tempY));
    }
    this->dataset.push_back(data);
    LineType LT;
    LT.type = type;
    LT.color = color;
    LT.is_need_lined = is_need_lined;
    this->lineTypeSet.push_back(LT);
    this->DrawData(this->Figure);
}


#endif /* CPlot_h */

