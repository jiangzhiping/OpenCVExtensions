//
//  AddedOperators.h
//  BinoCare
//
//  Created by JiangZhping on 2017/2/20.
//
//

#ifndef AddedOperators_h
#define AddedOperators_h

#include <iostream>
#include <opencv2/opencv.hpp>


#pragma mark - cv::Point and cv::Point3 operators
template<typename _Tp1, typename _Tp2> inline cv::Point_<_Tp1> operator + (const cv::Point_<_Tp1> & p1, const cv::Point_<_Tp2> & p2) {
    cv::Point_<_Tp1> p;
    p.x = cv::saturate_cast<_Tp1>(p1.x + cv::saturate_cast<_Tp1>(p2.x));
    p.y = cv::saturate_cast<_Tp1>(p1.y + cv::saturate_cast<_Tp1>(p2.y));
    return p;
}

template<typename _Tp1, typename _Tp2> inline cv::Point_<_Tp1> operator - (const cv::Point_<_Tp1> & p1, const cv::Point_<_Tp2> & p2) {
    cv::Point_<_Tp1> p;
    p.x = cv::saturate_cast<_Tp1>(p1.x - cv::saturate_cast<_Tp1>(p2.x));
    p.y = cv::saturate_cast<_Tp1>(p1.y - cv::saturate_cast<_Tp1>(p2.y));
    return p;
}

template<typename _Tp1, typename _Tp2> inline cv::Point_<_Tp1> operator + (const cv::Point3_<_Tp1> & p1, const cv::Point3_<_Tp2> & p2) {
    cv::Point_<_Tp1> p;
    p.x = cv::saturate_cast<_Tp1>(p1.x + cv::saturate_cast<_Tp1>(p2.x));
    p.y = cv::saturate_cast<_Tp1>(p1.y + cv::saturate_cast<_Tp1>(p2.y));
    p.z = cv::saturate_cast<_Tp1>(p1.z + cv::saturate_cast<_Tp1>(p2.z));
    return p;
}

template<typename _Tp1, typename _Tp2> inline cv::Point_<_Tp1> operator - (const cv::Point3_<_Tp1> & p1, const cv::Point3_<_Tp2> & p2) {
    cv::Point_<_Tp1> p;
    p.x = cv::saturate_cast<_Tp1>(p1.x - cv::saturate_cast<_Tp1>(p2.x));
    p.y = cv::saturate_cast<_Tp1>(p1.y - cv::saturate_cast<_Tp1>(p2.y));
    p.z = cv::saturate_cast<_Tp1>(p1.z - cv::saturate_cast<_Tp1>(p2.z));
    return p;
}

template<typename _Tp1, typename _Tp2> inline cv::Point_<_Tp1> & operator += (cv::Point_<_Tp1> & p1, const cv::Point_<_Tp2> & p2) {
    p1.x = cv::saturate_cast<_Tp1>(p1.x + cv::saturate_cast<_Tp1>(p2.x));
    p1.y = cv::saturate_cast<_Tp1>(p1.y + cv::saturate_cast<_Tp1>(p2.y));
    return p1;
}

template<typename _Tp1, typename _Tp2> inline cv::Point_<_Tp1> & operator -= (cv::Point_<_Tp1> & p1, const cv::Point_<_Tp2> & p2) {
    p1.x = cv::saturate_cast<_Tp1>(p1.x - cv::saturate_cast<_Tp1>(p2.x));
    p1.y = cv::saturate_cast<_Tp1>(p1.y - cv::saturate_cast<_Tp1>(p2.y));
    return p1;
}

template<typename _Tp1, typename _Tp2> inline cv::Point3_<_Tp1> & operator += (cv::Point3_<_Tp1> & p1, const cv::Point3_<_Tp2> & p2) {
    p1.x = cv::saturate_cast<_Tp1>(p1.x + cv::saturate_cast<_Tp1>(p2.x));
    p1.y = cv::saturate_cast<_Tp1>(p1.y + cv::saturate_cast<_Tp1>(p2.y));
    p1.z = cv::saturate_cast<_Tp1>(p1.z + cv::saturate_cast<_Tp1>(p2.z));
    return p1;
}

template<typename _Tp1, typename _Tp2> inline cv::Point3_<_Tp1> & operator -= (cv::Point3_<_Tp1> & p1, const cv::Point3_<_Tp2> & p2) {
    p1.x = cv::saturate_cast<_Tp1>(p1.x - cv::saturate_cast<_Tp1>(p2.x));
    p1.y = cv::saturate_cast<_Tp1>(p1.y - cv::saturate_cast<_Tp1>(p2.y));
    p1.z = cv::saturate_cast<_Tp1>(p1.z - cv::saturate_cast<_Tp1>(p2.z));
    return p1;
}

template<typename _Tp1, typename _Tp2> inline std::vector<cv::Point_<_Tp1>> & operator += (std::vector<cv::Point_<_Tp1>> & pointVector, const cv::Point_<_Tp2> & p) {
    for(auto & eachPoint: pointVector) {
        eachPoint += p;
    }
    return pointVector;
}

template<typename _Tp1, typename _Tp2> inline std::vector<cv::Point_<_Tp1>> & operator -= (std::vector<cv::Point_<_Tp1>> & pointVector, const cv::Point_<_Tp2> & p) {
    for(auto & eachPoint: pointVector) {
        eachPoint -= p;
    }
    return pointVector;
}

template<typename _Tp1, typename _Tp2> inline std::vector<cv::Point3_<_Tp1>> & operator += (std::vector<cv::Point3_<_Tp1>> & pointVector, const cv::Point3_<_Tp2> & p) {
    for(auto & eachPoint: pointVector) {
        eachPoint += p;
    }
    return pointVector;
}

template<typename _Tp1, typename _Tp2> inline std::vector<cv::Point3_<_Tp1>> & operator -= (std::vector<cv::Point3_<_Tp1>> & pointVector, const cv::Point3_<_Tp2> & p) {
    for(auto & eachPoint: pointVector) {
        eachPoint -= p;
    }
    return pointVector;
}


template<typename _Tp, int rows, int cols, typename _Tp2> inline
cv::Matx<_Tp, rows, cols> operator / (const cv::Matx<_Tp, rows, cols> & a, _Tp2 alpha)
{
    cv::Matx<_Tp, rows, cols> result;
    double ialpha = 1./alpha;
    for( int i = 0; i < rows * cols; i++ )
        result.val[i] = cv::saturate_cast<_Tp>(a.val[i]*ialpha);
    return result;
}

template<typename _Tp, int rows, int cols, typename _Tp2> inline
cv::Matx<_Tp, rows, cols>& operator /= (cv::Matx<_Tp, rows, cols>& a, _Tp2 alpha)
{
    double ialpha = 1./alpha;
    for( int i = 0; i < rows * cols; i++ )
        a.val[i] = cv::saturate_cast<_Tp>(a.val[i]*ialpha);
    return a;
}

template<typename _Tp> inline cv::Vec<_Tp, 2> operator + (const cv::Vec<_Tp, 2> & a, cv::Point_<_Tp> p) {
    return cv::Vec<_Tp, 2>(cv::saturate_cast<_Tp>(a(0) + p.x), cv::saturate_cast<_Tp>(a(1) + p.y));
}

template<typename _Tp> inline cv::Vec<_Tp, 2> operator - (const cv::Vec<_Tp, 2> & a, cv::Point_<_Tp> p) {
    return cv::Vec<_Tp, 2>(cv::saturate_cast<_Tp>(a(0) - p.x), cv::saturate_cast<_Tp>(a(1) - p.y));
}

template<typename _Tp> inline cv::Vec<_Tp, 3> operator + (const cv::Vec<_Tp, 3> & a, cv::Point3_<_Tp> p) {
    return cv::Vec<_Tp, 3>(cv::saturate_cast<_Tp>(a(0) + p.x), cv::saturate_cast<_Tp>(a(1) + p.y), cv::saturate_cast<_Tp>(a(2) + p.z));
}

template<typename _Tp> inline cv::Vec<_Tp, 3> operator - (const cv::Vec<_Tp, 3> & a, cv::Point3_<_Tp> p) {
    return cv::Vec<_Tp, 3>(cv::saturate_cast<_Tp>(a(0) - p.x), cv::saturate_cast<_Tp>(a(1) - p.y), cv::saturate_cast<_Tp>(a(2) - p.z));
}

template<typename _Tp> inline cv::Point_<_Tp> operator + (const cv::Point_<_Tp> & p, const cv::Vec<_Tp, 2> & a) {
    return cv::Point_<_Tp>(cv::saturate_cast<_Tp>(a(0) + p.x), cv::saturate_cast<_Tp>(a(1) + p.y));
}

template<typename _Tp> inline cv::Point_<_Tp> operator - (const cv::Point_<_Tp> & p, const cv::Vec<_Tp, 2> & a) {
    return cv::Point_<_Tp>(cv::saturate_cast<_Tp>(p.x - a(0)), cv::saturate_cast<_Tp>(p.y - a(1)));
}

template<typename _Tp> inline cv::Point3_<_Tp> operator + (const cv::Point3_<_Tp> & p, const cv::Vec<_Tp, 3> & a) {
    return cv::Point3_<_Tp>(cv::saturate_cast<_Tp>(a(0) + p.x), cv::saturate_cast<_Tp>(a(1) + p.y), cv::saturate_cast<_Tp>(a(2) + p.z));
}

template<typename _Tp> inline cv::Point3_<_Tp> operator - (const cv::Point3_<_Tp> & p, const cv::Vec<_Tp, 3> & a) {
    return cv::Point3_<_Tp>(cv::saturate_cast<_Tp>(p.x - a(0)), cv::saturate_cast<_Tp>(p.y - a(1)), cv::saturate_cast<_Tp>(p.z - a(2)));
}

template<typename _Tp> inline cv::Vec<_Tp, 2> & operator += (cv::Vec<_Tp, 2> & a, cv::Point_<_Tp> p) {
    a(0) = cv::saturate_cast<_Tp>(a(0) + p.x);
    a(1) = cv::saturate_cast<_Tp>(a(1) + p.y);
    return a;
}

template<typename _Tp> inline cv::Vec<_Tp, 2> & operator -= (cv::Vec<_Tp, 2> & a, cv::Point_<_Tp> p) {
    a(0) = cv::saturate_cast<_Tp>(a(0) - p.x);
    a(1) = cv::saturate_cast<_Tp>(a(1) - p.y);
    return a;
}

template<typename _Tp> inline cv::Vec<_Tp, 3> & operator += (cv::Vec<_Tp, 3> & a, cv::Point_<_Tp> p) {
    a(0) = cv::saturate_cast<_Tp>(a(0) + p.x);
    a(1) = cv::saturate_cast<_Tp>(a(1) + p.y);
    a(2) = cv::saturate_cast<_Tp>(a(2) + p.z);
    return a;
}

template<typename _Tp> inline cv::Vec<_Tp, 3> & operator -= (cv::Vec<_Tp, 3> & a, cv::Point_<_Tp> p) {
    a(0) = cv::saturate_cast<_Tp>(a(0) - p.x);
    a(1) = cv::saturate_cast<_Tp>(a(1) - p.y);
    a(2) = cv::saturate_cast<_Tp>(a(2) - p.z);
    return a;
}

template<typename _Tp> inline cv::Point_<_Tp> & operator += (cv::Point_<_Tp> & p, const cv::Vec<_Tp, 2> & a) {
    p.x = cv::saturate_cast<_Tp>(a(0) + p.x);
    p.y = cv::saturate_cast<_Tp>(a(1) + p.y);
    return p;
}

template<typename _Tp> inline cv::Point_<_Tp> & operator -= (cv::Point_<_Tp> & p, const cv::Vec<_Tp, 2> & a) {
    p.x = cv::saturate_cast<_Tp>(a(0) - p.x);
    p.y = cv::saturate_cast<_Tp>(a(1) - p.y);
    return p;
}

template<typename _Tp> inline cv::Point3_<_Tp> & operator += (cv::Point3_<_Tp> & p, const cv::Vec<_Tp, 3> & a) {
    p.x = cv::saturate_cast<_Tp>(a(0) + p.x);
    p.y = cv::saturate_cast<_Tp>(a(1) + p.y);
    p.z = cv::saturate_cast<_Tp>(a(2) + p.z);
    return p;
}

template<typename _Tp> inline cv::Point3_<_Tp> & operator -= (cv::Point3_<_Tp> & p, const cv::Vec<_Tp, 3> & a) {
    p.x = cv::saturate_cast<_Tp>(p.x - a(0));
    p.y = cv::saturate_cast<_Tp>(p.y - a(1));
    p.z = cv::saturate_cast<_Tp>(p.z - a(2));
    return p;
}

#endif /* AddedOperators_h */
