//
//  Singleton.hpp
//  OpenCVExtensions
//
//  Created by JiangZhping on 2017/2/1.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#ifndef Singleton_hpp
#define Singleton_hpp

#include <stdio.h>

template <class T>
class Singleton
{
public:
    template <typename... Args> static T& getInstance(Args... args) {
        if (!m_pInstance)
        {
            m_pInstance = new T(std::forward<Args>(args)...);
        }
        
        return *m_pInstance;
    }
protected:
    Singleton();
    ~Singleton();
private:
    Singleton(Singleton const&);
    Singleton& operator=(Singleton const&);
    static T* m_pInstance;
};

template <class T> T* Singleton<T>::m_pInstance= nullptr;


#endif /* Singleton_hpp */
