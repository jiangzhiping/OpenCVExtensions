#include <vector>
#include <opencv2/opencv.hpp>

class KalmanFilter
{
public:
	KalmanFilter() {};
	KalmanFilter(int number, int dtype);
	KalmanFilter(int number);
	KalmanFilter(int number, cv::Point2f);
	KalmanFilter(int number, cv::Point3f);
	KalmanFilter(int number, int dtype, cv::Point2f);
	KalmanFilter(int number, int dtype, cv::Point3f);
	~KalmanFilter();
	double processNoiseCov;
	double measurementNoiseCov;
	cv::Mat transitionMatrix;
	void setProcessNoiseCov(double value);
	void setMeasurementNoiseCov(double value);
	cv::Mat smooth(const cv::Mat & measurementMat);
	std::vector<cv::Point2f> smooth(const std::vector<cv::Point2f> & measurementPoints);
	std::vector<cv::Point3f> smooth(const std::vector<cv::Point3f> & measurementPoints);
private:
	cv::KalmanFilter * filter;
	int count;
	int stateNumber;
};

